from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoListForm

# Create your views here.
def todo_list_list(request):
    todo = TodoList.objects.all()
    context = {
        "todo_list_list": todo,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, pk):
    todo = get_object_or_404(TodoList, id=pk)
    context = {
        "todo_list_detail": todo,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo = form.save()
            return redirect("todo_list_detail", pk=todo.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)
